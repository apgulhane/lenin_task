var assert = require('chai').assert;
var { resolveDependency } = require('./app');

describe('resolveDependency()', function () {

    it('should solve an empty graph', function () {
        assert.deepEqual(resolveDependency({}), []);
    });
    
    it('should solve a valid graph', function () {
        let graph = {
            'C': ['B', 'A'],
            'D': ['B', 'A', 'C'],
            'A': [],
            'B': ['A'],
            'E': ['D', 'A', 'C', 'B'],
        }

        let solved = ['A', 'B', 'C', 'D', 'E'];

        assert.deepEqual(resolveDependency(graph), solved);
    });

    it('should solve a graph with missing keys', function () {
        let graph = {
            'A': ['B'],
            'B': ['C', 'D', 'E', 'F', 'G'],
        }

        let solved = ['C', 'D', 'E', 'F', 'G', 'B', 'A'];

        assert.deepEqual(resolveDependency(graph), solved);
    });

    

    
});