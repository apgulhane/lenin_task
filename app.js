let input = {
    A: ["B"],
    B: ["C"],
    C: ["D"]
}

function resolveDependency(inputJSON) {

    let graph = addMissingKeysToInput(inputJSON);

    let inDegrees = getinDegreesOfGraph(graph);

    let dependencyOrder = [];

    // Create a queue and add the vertices with in-degree 0
    let queue = [];

    for (let key in graph) {

        if (inDegrees[key] == 0) {
            queue.push(key);
        }

    }

    while (queue.length > 0) {
        
        // Shift the front of queue
        let elem = queue.shift();

        // enqueue to dependecy order
        dependencyOrder.unshift(elem);

        //decreased the indegree by 1
        for (let [key, value] of graph[elem].entries()) {
            inDegrees[value]--;
            
            // If in-degree is 0, enqueue it to queue
            if (inDegrees[value] == 0) {
                queue.unshift(value);
            }
        
        }

    }


    return dependencyOrder;

}

/**
 * 
 * @param {object} graph 
 * @return {object} graph with in degree of all vertices
 */
function addMissingKeysToInput(graph) {
    
    for (let key in graph) {
        for (let [index, value] of graph[key].entries()) {
            if (graph[value] === undefined) {
                graph[value] = [];
            }
        }
    }

    return graph;
}


/**
 * @param {object} graph 
 * @return {Map} object with indegrees of all vertices
 */
function getinDegreesOfGraph(graph) {
    
    let inDegrees = {};

    for (let key in graph) {
        for (let indeg of graph[key]) {
           
            inDegrees[indeg] = inDegrees[indeg] === undefined ? 1 : ++inDegrees[indeg];
            inDegrees[key] = inDegrees[key] || 0;
        
        }
    }

    return inDegrees;
}

//to test
console.log(resolveDependency(input));

module.exports = {
    resolveDependency
}